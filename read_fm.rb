require 'cmath'

include Math

I=Complex('i')
PI=Math::PI

def fft x
  n=x.size
  f=[]
  puts bar=(0..99).map{|i| (i%10==0) ? "|" : "-"}.join+"|"
  quantum=100.0/n
  percent=0
  for j in 0..n-1
    sum=0
    new=(j*quantum).to_i
    if new!=percent
      print '-'
      percent=new
    end
    for k in 0..n-1
      sum+=x[k]*E**(-2*PI*I*j*k/n)
    end
    f << sum
  end
  puts
  f
end

def fftshift x
  one= x.size.even? ? 0 : 1
  parts=x.each_slice( (x.size + one) / 2 ).to_a
  parts.reverse.flatten
end

def plot_fft_iq(x,n0,nf,fs,f0)
  puts "[+] plot"
  x_segment=x[n0..(n0+nf-1)]
  p=fftshift(fft(x_segment))
  max=p.map{|e| e.abs}.max
  z=p.map{|e| 20*log10(e.abs/max)}
  low_freq=(f0-fs/2.0)
  high_freq=(f0+fs/2.0)
  n=z.length
  freqs=(0..n-1).to_a.map{|e| (e*fs)/n + low_freq}
  File.open("xyp.data",'w'){|f|
    freqs.each_with_index do |freq,idx|
      f.puts "#{freq} #{z[idx]}"
    end
  }
  system("xyp -d /home/jcll/JCLL/dev/EXPERIMENTS/ruby_sdr/xyp.data")
end


def fir x,coeffs
  puts "      |-->[+] computing fir"
  y=[]
  puts (0..99).map{|e| e%10==0 ? '|' : '-'}.join + "|"
  quantum=100.0/x.size
  percent=0
  for n in 0..x.size-1
    sum=0
    new=(quantum*n).to_i
    if new!=percent
      percent=new
      print "."
    end
    for k in 0..29 # order 30
      sum+=coeffs[k]*x[n-k]
    end
    y << sum
  end
  puts
  y
end

def hamming n
  puts "      |-->[+] computing hamming window coefficients"
  w=[]
  for i in 0..n-1
    w[i]=0.54-0.46*cos(2*PI*i/n)
  end
  w
end

class Array
  def every(n)
    res=[]
    for i in 0..size-1
      if i%n==0
        res << self[i]
      end
    end
    res
  end
end

def decimate x,factor
  puts "[+] decimate by #{factor}"
  puts " |-->[+] fir with Hamming window"
  filtered=fir(x,hamming(x.size))
  puts " |-->[+] every #{factor}"
  filtered.every(factor)
end

def fm_iq_demod x
  puts "[+] iq demodulation"
  result=[]
  real=x.map{|c| c.real}
  imag=x.map{|c| c.imag}
  (2..x.size-1).each do |i|
    ampl=(imag[i]-imag[i-2])*real[i-1] - (real[i]-real[i-2])*imag[i-1]
    scaling=1.0/(real[i]**2+imag[i]**2)
    result << ampl*scaling
  end
  result
end

def to_mono x
  puts "[+] decimation to mono audio. fech=31250 Hz"
  decimate(x,10)
end

def normalize x
  puts "[+] normalize amplitude to [-1,+1]"
  max=x.map(&:abs).max
  x.map{|s| s/max}
end

def binary_encoding x
  puts "[+] binary encoding 32bits float"
  max=x.map(&:abs).max
  ratio=2**32/max
  y=x.map{|s| s*ratio}
  bin=y.pack("f*")
end

def save_file bin,filename="fm.raw"
  puts "[+] saving audio file '#{filename}'"
  File.open(filename,'wb'){|f| f.print(bin)}
end
#=====================================================
# script
#====================================================
filename= ARGV.first || "FMcapture1.dat"

puts "[+] reading file '#{filename}'"
s = File.open(filename,'rb'){|f| f.read}
y = s.unpack("C*")
#pp y[0..4].map{|e| e.to_s(16)}

puts "[+] converting to complex"
frac=1
y=y[0..y.size/frac] #
y=y.map{|u| u.to_f-127.5}
y=y.each_slice(2).to_a
y=y.map{|pair| Complex(pair[0],pair[1])}
# plot_fft_iq(y,1,0.002*2.5e6,2.5,100.122)

puts "[+] shifting for centering"

y_shifted=y.each_with_index.map{|s,i| s*E**(-i*I*2*PI*0.0712)}

#plot_fft_iq(y_shifted,1,0.002*2.5e6,2.5,101.1)

d= decimate(y_shifted,8)
#plot_fft_iq(d,1,0.002*2.5e6/8,2.5/8,100.122)

y_demodulated=fm_iq_demod(d)
#plot_fft_iq(y_demodulated,1,0.05*2.5e6/8,2.5/8,0)

df=to_mono(y_demodulated)
#plot_fft_iq(df,1,0.05*2.5e6/8/10,2.5/8/10,0)

normalized=normalize(df)
bin=binary_encoding(normalized)
save_file(bin,"fm.raw")
