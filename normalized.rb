
def normalize filename
  puts "[+] normalize '#{filename}'"
  x=IO.readlines(filename).map(&:to_f)
  max=x.map(&:abs).max
  ratio=32768.0/max
  y=x.map{|s| s*ratio}
  save_file(y,"fm_normalized.raw")
end

def save_file x,filename="fm.raw"
  puts "[+] saving audio file '#{filename}'"
  File.open(filename,'w') do |f|
    x.each do |val|
      f.puts val
    end
  end
end

filename=ARGV.first
normalize(filename)
