require_relative 'fft'

include Math

def gen_sin
  (0..2*PI).step(0.01).collect{|t| sin(4*t)}
end

# File.open("sin.txt",'w') do |f|
#   pairs.each do |pair|
#     x,y=*pair
#     f.puts "#{x} #{y}"
#   end
# end
#
# system("xyp -d sin.txt")
samples=gen_sin()[0..2**8-1]
p samples.size

freqs=fft(samples)
powers=freqs.map(&:abs2)

File.open("fft.txt",'w') do |f|
  powers.each_with_index do |ampl,freq|
    f.puts "#{freq} #{ampl}"
  end
end
system("xyp -d fft.txt")
